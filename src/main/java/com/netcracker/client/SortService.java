package com.netcracker.client;

import com.netcracker.shared.Book;
import com.netcracker.shared.BookList;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.RestService;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

public interface SortService extends RestService {
    @POST
    @Path("api/sort")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    void sortBook(Book book, MethodCallback<BookList> callback);
}
