package com.netcracker.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.ListDataProvider;
import com.netcracker.shared.Book;
import com.netcracker.shared.BookList;
import com.netcracker.shared.FieldVerifier;
import org.fusesource.restygwt.client.Defaults;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import java.util.List;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class gwt implements EntryPoint {
    /**
     * The message displayed to the user when the server cannot be reached or
     * returns an error.
     */

    private final Messages messages = GWT.create(Messages.class);
    private ListDataProvider<Book> dataProvider = new ListDataProvider<>();
    private List<Book> list = dataProvider.getList();

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        String root = Defaults.getServiceRoot();
        root = root.replace("gwt/", "");
        Defaults.setServiceRoot(root);
        //Формирование layout для размещения элементов
        DockLayoutPanel dockLayoutPanel = new DockLayoutPanel(Style.Unit.EM);
        VerticalPanel verticalPanel = new VerticalPanel();
        verticalPanel.setSpacing(10);

        //Формирование элементов
        final Button getButton = new Button(messages.getButton());
        final Button delButton = new Button(messages.delButton());
        final Button createButton = new Button(messages.createButton());
        final TextBox textAuthor = new TextBox();
        final TextBox textName = new TextBox();
        final TextBox textPages = new TextBox();
        final TextBox textPub = new TextBox();
        final TextBox textDate = new TextBox();
        final Label welcomeLabel = new Label("Список книг в библиотеке.");
        final Label errorLabel = new Label("");
        final Label idLabel = new Label("id удаляемой строчки: ");
        final ListBox listBox = new ListBox();
        final TextBox idDelete = new TextBox();
        final Label mark1 = new Label("Введите автора(только буквы)");
        final Label mark2 = new Label("Введите назвение (допускается любое)");
        final Label mark3 = new Label("Введите кол. страниц (цифры 10-5000)");
        final Label mark4 = new Label("Введите дату публикации (цифры 1500-2018)");
        final Label mark5 = new Label("Введите дату внесения в базу (дд-мм-гггг) не позднее (10-05-2018)");

        textAuthor.setEnabled(false);
        textName.setEnabled(false);
        textPages.setEnabled(false);
        textPub.setEnabled(false);
        textDate.setEnabled(false);
        createButton.setEnabled(false);
        delButton.setEnabled(false);
        idDelete.setEnabled(false);

        errorLabel.setStyleName("error");
        welcomeLabel.setStyleName("labelStyle");
        idDelete.setMaxLength(5);

        listBox.addItem("id");
        listBox.addItem("Автор");
        listBox.addItem("Название");
        listBox.addItem("Кол. стр.");
        listBox.addItem("Год изд.");

        listBox.setItemSelected(0, true);
        listBox.setEnabled(false);
        //Описание таблицы
        final CellTable<Book> table = new CellTable<>(10);

        TextColumn<Book> idColumn = new TextColumn<Book> () {
            @Override
            public String getValue (Book book){
                return "" + book.getId();
            }
        };


        TextColumn<Book> authorColumn = new TextColumn<Book>() {
            @Override
            public String getValue (Book book){
                return "" + book.getAuthor();
            }
        };


        TextColumn<Book>nameColumn = new TextColumn<Book> () {
            @Override
            public String getValue (Book book){
                return "" + book.getName();
            }
        };


        TextColumn<Book> pagesColumn = new TextColumn<Book>() {
            @Override
            public String getValue (Book book){
                return "" + book.getPages();
            }
        };


        TextColumn<Book> yearColumn = new TextColumn<Book>() {
            @Override
            public String getValue (Book book){
                return "" + book.getYear();
            }
        };


        TextColumn<Book> dateColumn = new TextColumn<Book>() {
            @Override
            public String getValue (Book book){
                return "" + book.getDate();
            }
        };

        table.addColumn(idColumn, "id");
        table.addColumn(authorColumn, "Автор");
        table.addColumn(nameColumn, "Название");
        table.addColumn(pagesColumn, "Кол. стр.");
        table.addColumn(yearColumn, "Год изд.");
        table.addColumn(dateColumn, "Дата добав.");
        //Ввыод таблицы на экран
        dataProvider.addDataDisplay(table);
        SimplePager pager = new SimplePager();
        pager.setPageSize(10);
        pager.setPageStart(0);
        pager.setDisplay(table);
        VerticalPanel vTable = new VerticalPanel();
        vTable.add(table);
        vTable.add(pager);
        //Добавление элементов на страницу
        dockLayoutPanel.addNorth(welcomeLabel, 4);
        verticalPanel.add(getButton);
        verticalPanel.add(new Label("Сортировака по:"));
        verticalPanel.add(listBox);
        verticalPanel.add(delButton);
        verticalPanel.add(idLabel);
        verticalPanel.add(idDelete);
        verticalPanel.add(createButton);
        verticalPanel.add(mark1);
        verticalPanel.add(textAuthor);
        verticalPanel.add(mark2);
        verticalPanel.add(textName);
        verticalPanel.add(mark3);
        verticalPanel.add(textPages);
        verticalPanel.add(mark4);
        verticalPanel.add(textPub);
        verticalPanel.add(mark5);
        verticalPanel.add(textDate);
        dockLayoutPanel.addWest(verticalPanel, 15);
        dockLayoutPanel.addSouth(errorLabel, 5);
        dockLayoutPanel.add(vTable);
        RootLayoutPanel.get().add(dockLayoutPanel);
        RootLayoutPanel.get().setStyleName("body");

        class GetHandler implements ClickHandler {
            public void onClick(ClickEvent event) {
                BookService service = GWT.create(BookService.class);
                service.call(new MethodCallback<BookList>() {
                    @Override
                    public void onSuccess(Method method, BookList response) {
                        list.clear();
                        List<Book> bookList = response.getBookList();
                        list.addAll(bookList);
                        errorLabel.setText("");

                        idDelete.setEnabled(true);
                        delButton.setEnabled(true);
                        createButton.setEnabled(true);
                        textAuthor.setEnabled(true);
                        textName.setEnabled(true);
                        textPages.setEnabled(true);
                        textPub.setEnabled(true);
                        textDate.setEnabled(true);
                        listBox.setEnabled(true);
                    }

                    public void onFailure(Method method, Throwable exception) {
                        errorLabel.setText("Ошибка получения данных с сервера");
                    }
                });
            }
        }

        GetHandler getHandler = new GetHandler();
        getButton.addClickHandler(getHandler);

        class DeleteHandler implements ClickHandler, KeyUpHandler {
            private DeleteService service = GWT.create(DeleteService.class);

            public void onClick(ClickEvent event) {
                helper();
            }

            public void onKeyUp(KeyUpEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    helper();
                }
            }

            private boolean idValid() {

                errorLabel.setText("");
                String id = idDelete.getText();
                if (!FieldVerifier.isValidId(id, list.size())) {
                    errorLabel.setText("Неверный ввод id. Требуется число, не превышающее диапазон id в таблице");
                    return false;
                }
                return true;
            }

            private void helper() {
                if (idValid()) {
                    int id = Integer.parseInt(idDelete.getText());
                    Book book = new Book();
                    book.setId(id);
                    service.deleteBook(book, new MethodCallback<BookList>() {
                        @Override
                        public void onFailure(Method method, Throwable exception) {
                            errorLabel.setText("Ошибка при работе сервера");
                        }

                        @Override
                        public void onSuccess(Method method, BookList response) {
                            errorLabel.setText("");
                            list.clear();
                            List<Book> bookList = response.getBookList();
                            list.addAll(bookList);
                            if (list.size() == 0 || list.size() == 1) {
                                delButton.setEnabled(false);
                                idDelete.setEnabled(false);
                            }
                            idDelete.setText("");
                        }
                    });
                }
            }
        }

        DeleteHandler deleteHandler = new DeleteHandler();
        delButton.addClickHandler(deleteHandler);
        idDelete.addKeyUpHandler(deleteHandler);

        class Checker {
            private boolean check1 = false, check2 = false, check3 = false, check4 = false, check5 = false;
            boolean isCheck1() {
                return check1;
            }
            void setCheck1(boolean check1) {
                this.check1 = check1;
            }
            boolean isCheck2() {
                return check2;
            }
            void setCheck2(boolean check2) {
                this.check2 = check2;
            }
            boolean isCheck3() {
                return check3;
            }
            void setCheck3(boolean check3) {
                this.check3 = check3;
            }
            boolean isCheck4() {
                return check4;
            }
            void setCheck4(boolean check4) {
                this.check4 = check4;
            }
            boolean isCheck5() {
                return check5;
            }
            void setCheck5(boolean check5) {
                this.check5 = check5;
            }
        }

        Checker checker = new Checker();


        class AuthHandler implements KeyUpHandler {
            public void onKeyUp(KeyUpEvent event) {
                errorLabel.setText("");
                valid();

            }

            private void valid() {
                checker.setCheck1(true);
                if (!FieldVerifier.authValid(textAuthor.getText())) {
                    errorLabel.setText("Неверный ввод автора. Допускаются только буквы. Для отправления заполните всю форму и нажмите кнопку отправки");
                    checker.setCheck1(false);
                }
            }
        }
        textAuthor.addKeyUpHandler(new AuthHandler());

        class NameHandler implements KeyUpHandler {
            public void onKeyUp(KeyUpEvent event) {
                errorLabel.setText("");
                valid();
            }

            private void valid() {
                checker.setCheck2(true);
                if (!FieldVerifier.nameValid(textName.getText())) {
                    errorLabel.setText("Неверный ввод названия. Не допускается пустое поле. Для отправления заполните всю форму и нажмите кнопку отправки");
                    checker.setCheck2(false);
                }
            }
        }
        textName.addKeyUpHandler(new NameHandler());

        class PagesHandler implements KeyUpHandler {
            public void onKeyUp(KeyUpEvent event) {
                errorLabel.setText("");
                valid();
            }

            private void valid() {
                checker.setCheck3(true);
                if (!FieldVerifier.intValid(textPages.getText(), 10, 5000)) {
                    errorLabel.setText("Неверный ввод количества страниц. Допускаются только цифры в указанном диапазоне. Для отправления заполните всю форму и нажмите кнопку отправки");
                    checker.setCheck3(false);
                }
            }
        }
        textPages.addKeyUpHandler(new PagesHandler());

        class PublishHandler implements KeyUpHandler {
            public void onKeyUp(KeyUpEvent event) {
                errorLabel.setText("");
                valid();
            }

            private void valid() {
                checker.setCheck4(true);
                if (!FieldVerifier.intValid(textPub.getText(), 1500, 2018)) {
                    errorLabel.setText("Неверный ввод даты публикации. Допускаются только цифры в указанном диапазоне. Для отправления заполните всю форму и нажмите кнопку отправки");
                    checker.setCheck4(false);
                }
            }
        }
        textPub.addKeyUpHandler(new PublishHandler());

        class DateHandler implements KeyUpHandler {
            public void onKeyUp(KeyUpEvent event) {
                errorLabel.setText("");
                valid();
            }

            private void valid() {
                checker.setCheck5(true);
                if (!FieldVerifier.dateValid(textDate.getText())) {
                    errorLabel.setText("Неверный ввод даты. Соблюдайте формат. Для отправления заполните всю форму и нажмите кнопку отправки");
                    checker.setCheck5(false);
                }
            }
        }
        textDate.addKeyUpHandler(new DateHandler());

        class CreateHandler implements ClickHandler {
            @Override
            public void onClick(ClickEvent event) {
                CreateService service = GWT.create(CreateService.class);
                if (checker.isCheck1() && checker.isCheck2() && checker.isCheck3() && checker.isCheck4() && checker.isCheck5()) {
                    Book book = new Book();
                    book.setAuthor(textAuthor.getText());
                    book.setName(textName.getText());
                    book.setPages(Integer.parseInt(textPages.getText()));
                    book.setYear(Integer.parseInt(textPub.getText()));
                    book.setDate(textDate.getText());
                    service.createBook(book, new MethodCallback<BookList>() {
                        @Override
                        public void onFailure(Method method, Throwable exception) {
                            errorLabel.setText("Ошибка при работе сервера");
                        }

                        @Override
                        public void onSuccess(Method method, BookList response) {
                            errorLabel.setText("");
                            list.clear();
                            List<Book> bookList = response.getBookList();
                            list.addAll(bookList);
                            if (list.size() > 1) {
                                delButton.setEnabled(true);
                                idDelete.setEnabled(true);

                            }
                            textAuthor.setText("");
                            textName.setText("");
                            textPages.setText("");
                            textPub.setText("");
                            textDate.setText("");
                        }
                    });
                } else errorLabel.setText("Незаполены, либо неверно заполнены поля");
            }
        }
        createButton.addClickHandler(new CreateHandler());

        class SortHandler implements ChangeHandler {
            @Override
            public void onChange(ChangeEvent event) {
                Book book = new Book();
                book.setId(listBox.getSelectedIndex());
                SortService service = GWT.create(SortService.class);
                service.sortBook(book, new MethodCallback<BookList>() {
                    @Override
                    public void onFailure(Method method, Throwable exception) {
                        errorLabel.setText("Ошибка при работе сервера");
                    }

                    @Override
                    public void onSuccess(Method method, BookList response) {
                        errorLabel.setText("");
                        list.clear();
                        List<Book> bookList = response.getBookList();
                        list.addAll(bookList);
                    }
                });
            }
        }
        listBox.addChangeHandler(new SortHandler());

    }
}
